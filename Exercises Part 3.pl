% Prolog Exercises Part 3

% Ex 3.1

% same(List1,List2)
% are the two lists the same?
same([],[]).
same([X|Xs],[X|Ys]):- same(Xs,Ys).

% Ex 3.2

% all_bigger(List1,List2)
% all elements in List1 are bigger than those in List2, 1 by 1
% example: all_bigger([10,20,30,40],[9,19,29,39]).
all_bigger([X],[Y]) :- X > Y.
all_bigger([X|Xs],[Y|Ys]) :- X > Y, all_bigger(Xs,Ys).

% Ex 3.3

% sublist(List1,List2)
% List1 should be a subset of List2
% example: sublist([1,2],[5,3,2,1]).
sublist([],[]).
sublist([],[X]).
sublist([X],[X|_]).
sublist([X],[_|Ys]) :- search(X,Ys).
sublist([X|Xs],[X|Ys]) :- sublist(Xs,[X|Ys]).
sublist([X|Xs],[Y|Ys]) :- search(X,Ys), sublist(Xs,[Y|Ys]).

search(X,[X|_]).
search(X,[_|Xs]):-search(X,Xs).
