% Prolog Exercises Part 1

% Ex 1.1

% look for occurrences of Elem
% search(Elem, List)
search(X,[X|_]).
search(X,[_|Xs]):-search(X,Xs).

% Ex 1.2

% look for two consecutive occurrences of Elem
% search2(Elem, List)
search2(X,[X,X|_]).
search2(X,[_|Xs]):-search2(X,Xs).

% Ex 1.3

% look for two occurrences of Elem with an element in between!
% search_two(ELem, List)
search_two(X, [X,_,X|_]).
search_two(X, [_|Xs]) :- search_two(X,Xs).

% Ex 1.4

% look for any Elem that occurs two times
% search_anytwo(Elem, List)
search_anytwo(X, [X|Xs]) :- search(X,Xs).
search_anytwo(X, [_|Xs]) :- search_anytwo(X,Xs).

