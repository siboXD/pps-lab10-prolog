% Prolog Exercises Part 4

% Ex 4.1

% seq(N,List)
% example: seq(5,[0,0,0,0,0]).
seq(0,[]).
seq(N,[0|T]):- N > 0, N2 is N-1, seq(N2,T).

% Ex 4.2

% seqR(N,List)
% example: seqR(4,[4,3,2,1,0]).

seqR(0,[0]).
seqR(N,[N|T]):- N > 0, N2 is N-1, seqR(N2,T).

% Ex 4.3

% seqR2(N,List)
% example: seqR2(4,[0,1,2,3,4]).

seqR2(0,[0]).
seqR2(N,[X|Xs]):- N > 0, N2 is N-1, seqR2(N2,[X|Ys]), last(Ys,N,Xs).

last([],X,[X]).
last([X|Xs],Y,[X|Ys]):- last(Xs,Y,Ys).
